// Fetch request using GET method
fetch("https://jsonplaceholder.typicode.com/todos", {method: "GET"})
.then(response => response.json());

// Title of every item using map method
fetch("https://jsonplaceholder.typicode.com/todos")
.then(response => response.json())
.then(data => {
	let items = data.map(element => element.title);
	console.log(items);
});

// Print a single to do list item using GET method
fetch("https://jsonplaceholder.typicode.com/todos/1", { method: "GET" })
.then(response => response.json())
.then(result => console.log(result));

// Print a message providing the title and status
fetch("https://jsonplaceholder.typicode.com/todos/1", { method: "GET" })
.then(response => response.json())
.then(result => console.log(`The item "${result.title}" on the list has a status of ${result.completed}`));

// Create a to do list item using POST method
fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
			"userId": 1,
			"title": "Created To Do List Item",
			"completed": false
		})
})
.then(response => response.json())
.then(result => console.log(result));

// Update a to do list item using PUT method
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
			"userId": 1,
			"title": "Updated To Do List Item",
			"completed": "Pending"
		})
})
.then(response => response.json())
.then(result => console.log(result));

// Update a to do list item by changing the data structure
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
			"title": "Updated To Do List Item",
			"description": "To update my to do list with a different data structure",
			"status": "Pending",
			"dateCompleted": "Pending",
			"userId": 1
		})
})
.then(response => response.json())
.then(result => console.log(result));

// Update a to do list item using PATCH method
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH",
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
			"title": "delectus aut autem",
		})
})
.then(response => response.json())
.then(result => console.log(result));

// Update a to do list item changing status and adding a date
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH",
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
			"status": "Complete",
			"dateCompleted": "07/09/21"
		})
})
.then(response => response.json())
.then(result => console.log(result));

// Delete an item using DELETE method
fetch("https://jsonplaceholder.typicode.com/todos/201", {method: "DELETE"})
.then(response => response.json());
.then(result => console.log(result));